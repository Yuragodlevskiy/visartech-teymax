import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/functions'

const config = {
  apiKey: 'AIzaSyAkeI2SUhaEZXkA3ZLeFhcp40biynVYxgY',
  databaseURL: 'https://visartech-website.firebaseio.com/',
  projectId: 'visartech-website',
  messagingSenderId: '106352880821'
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

export const fireDb = firebase.database()
export const fireFunc = firebase.functions()
