import Vue from 'vue'
import vClickOutside from 'v-click-outside'
import * as VueGoogleMaps from 'vue2-google-maps'
import VTLink from '@/components/common/Link'
import MediaPopup from '@/components/common/MediaPopup'
import ContactForm from '@/components/common/ContactForm'
import LazyPicture from '@/components/common/LazyPicture'

Vue.use(vClickOutside)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAkeI2SUhaEZXkA3ZLeFhcp40biynVYxgY',
    libraries: 'places' // necessary for places input
  },
  installComponents: true
})

Vue.component('VTLink', VTLink)
Vue.component('MediaPopup', MediaPopup)
Vue.component('ContactForm', ContactForm)
Vue.component('LazyPicture', LazyPicture)
