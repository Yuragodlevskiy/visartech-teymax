import Vue from 'vue'

import format from 'date-fns/format'

const dateFilter = (date) => {
  return format(new Date(date), 'MMMM D, YYYY')
}

Vue.filter(dateFilter)
