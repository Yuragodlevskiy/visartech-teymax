import axios from 'axios'
import { fireDb } from './plugins/firebase'

const API_URL = 'https://admin-pgames.c.wpstage.net/wp-json'

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, viewport-fit=cover'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css'
      },
      {
        type: 'text/javascript',
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.jsylesheet/css'
      }
    ],
    script: [
      { src: '/js/gsap.min.js' },
      { src: '/js/main.js', body: true },
      { src: '/js/show.js', body: true },
      { src: '/js/animation.js', body: true },
      { src: '/js/menu.js', body: true }
    ],
    noscript: [{ innerHTML: 'This website requires JavaScript.' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['~assets/css/style.css', '~assets/scss/core/_base.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~plugins/date.filter',
    '~plugins/error.filter',
    '~plugins/register-components',
    '~plugins/axios',
    '~plugins/firebase'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/gtm'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'vue-social-sharing/nuxt',
    '@nuxtjs/sitemap'
  ],
  env: {
    // The Site URL which should be the same as set in Wordpress.
    // It's used to transform absolute urls from WP API to relative links for nuxt-link.
    wpSiteUrl: process.env.wpSiteUrl || 'https://visartech.com/',
    cacheAPIRequests: process.env.cacheAPIRequests || false,
    siteUrl: process.env.siteUrl || 'https://visartech.com'
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: { baseURL: API_URL },
  gtm: {
    enabled: true,
    id: 'GTM-PSFCHML',
    pageTracking: true
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    transpile: [/^vue2-google-maps($|\/)/],
    extend(config, { isDev, isClient }) {
      if (isClient) {
        config.node = {
          fs: 'empty',
          child_process: 'empty'
        }
      }
    },
    postcss: {
      plugins: {
        'postcss-import': {},
        'postcss-url': {},
        'postcss-preset-env': this.preset,
        cssnano: { preset: 'default' } // disabled in dev mode
      }
    }
  },
  generate: {
    routes(callback) {
      axios
        .get(API_URL + '/wp/v2/posts?per_page=100')
        .then((res) => {
          const routes = res.data.map((post) => '/blog/' + post.slug + '/')
          callback(null, routes)
        })
        .catch(callback)
    }
  },
  router: {
    trailingSlash: true
  },
  sitemap: {
    hostname: 'https://visartech.com',
    routes: async () => {
      const { data } = await axios.get(API_URL + '/wp/v2/posts?per_page=100')
      const result = data.map((post) => ({
        url: '/blog/' + post.slug + '/',
        priority: 0.9
      }))
      result.push(
        {
          url: '/',
          priority: 0.7
        },
        {
          url: '/portfolio/',
          priority: 0.8
        },
        {
          url: '/services/',
          priority: 1
        },
        {
          url: '/blog/',
          priority: 0.9,
          changefreq: 'daily'
        },
        {
          url: '/contacts/',
          priority: 0.6
        },
        {
          url: '/services/interactive-app/'
        },
        {
          url: '/services/digital-art/'
        },
        {
          url: '/services/extended-reality/'
        },
        {
          url: '/services/video-games/'
        },
        {
          url: '/services/software-product/'
        },
        {
          url: '/businesses/'
        }
      )
      return result
    },
    exclude: ['/404/', '/cookie-policy/', '/privacy-policy/'],
    defaults: {
      changefreq: 'weekly',
      lastmod: new Date()
    }
  },
  hooks: {
    generate: {
      done() {
        fireDb.goOffline()
      }
    }
  }
}
