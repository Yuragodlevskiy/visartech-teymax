import { getMatchedComponents } from './utils'

export default function(to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false
  let targetY = 0
  let animate = false

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  const nuxt = window.$nuxt
  const pages = getMatchedComponents(to)

  // triggerScroll is only fired when a new component is loaded
  // so fire triggerScroll manually for 404 page and for transitions on same page
  if (pages.length === 0) {
    nuxt.$nextTick(() => nuxt.$emit('triggerScroll'))
  } else if (to.path === from.path && (to.hash || from.hash)) {
    animate = true
    nuxt.$nextTick(() => nuxt.$emit('triggerScroll'))
  }

  return new Promise((resolve) => {
    // wait for the out transition to complete (if necessary)
    nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (
          typeof window.CSS !== 'undefined' &&
          typeof window.CSS.escape !== 'undefined'
        ) {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }

        let scrollTo = null
        try {
          scrollTo = document.querySelector(hash)
          // Get the top position of an element in the document
          // return value of html.getBoundingClientRect().top ... IE : 0, other browsers : -pageYOffset
          targetY =
            scrollTo.nodeName === 'HTML'
              ? -window.pageYOffset
              : scrollTo.getBoundingClientRect().top + window.pageYOffset

          // Ajusts offset from the end
          targetY -= 100
          animate = true
        } catch (e) {
          console.warn(
            'Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).'
          )
        }
      }

      if (position) {
        resolve(position)
        return
      }

      if (!animate) {
        position = { x: 0, y: targetY }
        resolve(position)
        return
      }

      // scroll to anchor by returning the selector
      smoothScroll(targetY, 1000, () => {
        position = { x: 0, y: targetY }
        resolve(position)
      })
    })
  })
}

function smoothScroll(targetY, duration, callback) {
  const clock = Date.now()

  // we use requestAnimationFrame to be called by the browser before every repaint
  const requestAnimationFrame =
    window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    function(fn) {
      window.setTimeout(fn, 15)
    }

  const step = function() {
    // the time elapsed from the beginning of the scroll
    const elapsed = Date.now() - clock
    // calculate the scroll position we should be in
    let y = targetY
    if (elapsed < duration) {
      y =
        window.pageYOffset +
        (targetY - window.pageYOffset) * easeInOutCubic(elapsed / duration)
      requestAnimationFrame(step)
      window.scroll(0, y)
    } else {
      window.scroll(0, targetY)
      callback()
    }
  }
  step()
}

// ease in out function thanks to:
// http://blog.greweb.fr/2012/02/bezier-curve-based-easing-functions-from-concept-to-implementation/
function easeInOutCubic(t) {
  return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1
}
