var curentMouseTargetServices = false,
  curentMouseTargetSearch = false,
  dropdown = document.getElementById("services_dropdown"),
  searchDropdown = document.getElementById("search_dropdown"),
  categoryItemArray = [],
  news = document.getElementById("services_news"),
  searchBlock = document.getElementById("search_dropdown_block"),
  searchImg = document.getElementById("search_dropdown_img"),
  searchAfter = document.getElementById("search_dropdown_after"),
  searchInput = document.getElementById("search_dropdown_input"),
  bgSliderSwitchers = document.querySelectorAll("[bgSliderSwitcher]"),
  bgSliderParent = document.getElementById("bgSlider"),
  bgSliderSwitchersClasses = [],
  sorting_btn = document.querySelectorAll(".sorting__item");

function scrollToAnchor(element){
  let headerHeight = document.getElementById('menu')?document.getElementById('menu'):document.getElementById('blackMenu');
  headerHeight = headerHeight.offsetHeight;
  element = document.getElementById(element).getBoundingClientRect().top + pageYOffset;
  window.scrollTo(0, element-headerHeight)
}

class BgSlider {
  constructor(parent, smallText, hoveredText, line) {
    this.parent = parent;
    this.smallText = smallText;
    this.hoveredText = hoveredText;
    this.line = line;
  }

  overAnimation() {
    overBgSwitchAnimation(this);
  }

  toggleBlock(target, buttons, classes) {
    target = target.closest("[bgSliderSwitcher]");
    classes.forEach((item, i) => {
      let consistClass = item.parent.className.indexOf(" active");
      item.parent.classList.remove("active");
      if (item.parent == target && consistClass == -1) {
        item.parent.classList.add("active");
        setTimeout(() => {
          showBgSwitchAnimation(item);
        }, 300);
      } else if (item.parent != target && consistClass > -1) {
        item.parent.classList.remove("active");
        hideBgSwitchAnimation(item);
      } else {
        console.log("empty");
      }
    });
  }

  outAnimation() {
    outBgSwitchAnimation(this);
  }
}

var switchersOffer = document.querySelectorAll(".our_offering__switcher_item");

switchersOffer.forEach((item, i) => {
  item.addEventListener("mouseover", e => {
    switchersOffer.forEach((item, i) => {
      item.classList.remove("active");
    });

    item.classList.toggle("active");
  });
});

var swtichedBg = false;

bgSliderSwitchers.forEach((item, i) => {
  let slider = new BgSlider(
    item,
    item.querySelectorAll(".bg-slider__switcher_item"),
    item.querySelectorAll(".bg-slider__switcher_item_hovered"),
    item.querySelectorAll(".bg-slider__switcher_item_before")
  );

  bgSliderSwitchersClasses.push(slider);

  slider.parent.addEventListener("click", e => {
    slider.toggleBlock(e.target, bgSliderSwitchers, bgSliderSwitchersClasses);
    slider.parent.classList.add("active");
  });
});

window.onload = () => {
  (category = document.getElementById("services_cat")),
    (categoryItems = category.querySelectorAll(
      ".menu_services__categories_item"
    ));

  categoryItems.forEach((item, i) => {
    categoryItemArray.push(item);
  });
};

function clickSwitch(e) {
  document
    .getElementById("slide__switcher")
    .querySelectorAll(".slide__switcher_item")
    .forEach((item, index) => {
      item.classList.remove("active");
      if (item.contains(e.target)) {
        item.classList.add("active");
      }
    });
}

function openSearch(e) {
  if (!curentMouseTargetSearch) {
    closeDropdown();
    openSearchAnimate();
    setTimeout(() => {
      document.addEventListener("click", closeMenu);
    }, 20);
  }
  curentMouseTargetSearch = true;
}

function closeSearch(e) {
  curentMouseTargetSearch = false;
  closeSearchAnimate();
  document.removeEventListener("click", closeMenu);
}

function closeMenu(e) {
  let target = e.target,
    its_dropbox =
      target == searchDropdown
        ? true
        : findChild(searchDropdown, target)
        ? true
        : closeSearch();
}

function findChild(parent, selector) {
  let allChild = parent.children;
  for (var i = 0; i < allChild.length; i++) {
    if (allChild[i] == selector) {
      return true;
    }
    if (allChild[i].hasChildNodes()) {
      if (findChild(allChild[i], selector)) {
        return true;
      }
    }
  }
  return false;
}

function openReview(data) {
  let photos = document.getElementById("photo"),
    block = document.getElementById("photo" + data),
    text = document.getElementById("name" + data);

  photos.style.marginLeft = "-" + data * 109 + "px";
  for (var i = 0; i < 4; i++) {
    document.getElementById("photo" + i).classList.remove("active");
    document.getElementById("name" + i).style.display = "none";
    document.getElementById("review" + i).style.display = "none";
    document.getElementById("position" + i).style.display = "none";
  }

  block.classList.add("active");
  text.style.display = "flex";
  document.getElementById("review" + data).style.display = "flex";
  document.getElementById("position" + data).style.display = "flex";
}

var activeReview = 0;

function reviewSlider(data) {
  activeReview = activeReview + data;
  activeReview > 3 ? (activeReview = 0) : 1;
  activeReview < 0 ? (activeReview = 3) : 1;

  for (var i = 0; i < 4; i++) {
    document.getElementById("photo" + i).classList.remove("active");
    document.getElementById("name" + i).style.display = "none";
    document.getElementById("review" + i).style.display = "none";
    document.getElementById("position" + i).style.display = "none";
  }

  document.getElementById("photo" + activeReview).classList.add("active");
  document.getElementById("name" + activeReview).style.display = "flex";
  document.getElementById("review" + activeReview).style.display = "flex";
  document.getElementById("position" + activeReview).style.display = "flex";
}

sorting_btn.forEach((item, i) => {
  item.addEventListener("click", e => {
    e.target.parentNode.classList.toggle("active");
  });
});

document.querySelectorAll(".portfolio-list_item_more").forEach((element, i) => {
  element.addEventListener("click", e => {
    document.querySelectorAll(".portfolio-list_item").forEach((item, i) => {
      item.classList.remove("active");
      if (findChild(item, e.target)) {
        item.classList.toggle("active");
      }
    });
  });
});

var packets = document.querySelectorAll("[packet]");

packets.forEach((item, i) => {
  item.addEventListener("click", e => {
    packets.forEach((item, i) => {
      item.classList.remove("active");
      if (findChild(item, e.target) || item == e.target) {
        item.classList.toggle("active");
      }
    });
  });
});

// TODO: develop handler for mouse event
