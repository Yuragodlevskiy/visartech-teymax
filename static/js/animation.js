var searchOpenTimeLine = new TimelineMax(),
    searchCloseTimeLine = new TimelineMax(),
    servicesOpenTimeLine = new TimelineMax({onComplete : ()=>{
      curentMouseTargetServices = true;
    }}),
    servicesCloseTimeLine = new TimelineMax({onComplete : ()=>{
      curentMouseTargetServices = false;
    }})

var width = window.innerWidth;


function openDropdownAnimate(){
  category = document.getElementById('services_cat')
  console.log('start show')
  document.getElementById('services_menu').classList.add('active')
  servicesOpenTimeLine.add(TweenMax.to([dropdown, news, category], 0.02, {
    display: 'flex',
  }))

  servicesOpenTimeLine.to(document.getElementById('services_dropdown'), 0.2, {
    height: '300px',
    padding: '50px 0 110px 0'
  })
  servicesOpenTimeLine.to(document.getElementById('services_sub'), 0.02, {
    display: "flex"
  })
  servicesOpenTimeLine.to([document.getElementById('services_dropdown'), document.getElementById('services_dropdown')], 0.1, {
    opacity: "1"
  })

  servicesOpenTimeLine.staggerTo(categoryItemArray, 0.1, {
    opacity: "1",
    top: "0px"
  }, 0.05).eventCallback("onComplete", ()=>{
    curentMouseTargetServices = true;
    animationPlay = false;
  });

  servicesOpenTimeLine.call(()=>{
    subcategory.classList.add('active')
  }, null, null, "+=0.01")
}

function closeDropdownAnimate(){
  document.getElementById('services_menu').classList.remove('active')
  console.log('end show')
  servicesCloseTimeLine.call(()=>{
    subcategory.classList.remove('active')
  }, null, null, 0.2)

  servicesCloseTimeLine.to(subcategory, 0.02, {
    display: 'none',
    delay: '0.3'
  })

  servicesCloseTimeLine.staggerTo(categoryItemArray, 0.05, {
    opacity: "0",
    top: "-30px"
  }, 0.05)


  servicesCloseTimeLine.to([news, category], 0.05, {
    opacity: '0',
    display: 'none'
  })

  servicesCloseTimeLine.add(TweenMax.to(dropdown, 0.1, {
    height: '0px',
    padding: '0',
    display: 'none',
  })).eventCallback("onComplete", ()=>{
    curentMouseTargetServices = false;
    animationPlay = false;
  });
}

function openSearchAnimate(){
  searchOpenTimeLine.to(searchDropdown, 0.3, {
    display: 'flex',
    height: "100px",
    paddingTop: "40px",
    paddingBottom: "40px"
  });
  searchOpenTimeLine.to([searchImg,searchInput,searchBlock] , 0.3, {
    opacity: "1",
    top: "0px"
  });
}

function closeSearchAnimate(){
  searchCloseTimeLine.to([searchImg,searchInput,searchBlock] , 0.3, {
    opacity: "0",
    top: "-30px"
  });
  searchCloseTimeLine.to(searchDropdown , 0.3, {
    height: "0",
    paddingTop: "0",
    paddingBottom: "0",
    display: 'none'
  });
}

function showFromBottom(element, time, delay){
  time = time/1000;
  delay = delay/1000;
  TweenMax.fromTo(element, time, {
    y: "+50",
    opacity: '0'
  }, {
    y: "0",
    opacity: '1',
    delay: '' + delay
  })
}

function showFromTop(element, time, delay){
  time = time/1000;
  delay = delay/1000;
  TweenMax.fromTo(element, time, {
    y: "-50",
    opacity: '0'
  }, {
    y: "0",
    opacity: '1',
    delay: '' + delay
  })
}

function showBgSwitchAnimation(switcher){
  var switchHover = new TimelineMax();
  console.log('start show')
  switchHover.to(switcher.smallText, 0.1, {
    opacity: "0",
    y: "-50",
    display: "none"
  });

  switchHover.to(switcher.parent, 0.1, {
    height: 'fit-content',
    marginTop: '15px'
  }, "-=0.2");

  if (width>680){
    switchHover.to(switcher.parent, 0.1, {
      height: '80'
    }, "-=0.2");
  } else {
    switchHover.to(switcher.parent, 0.1, {
      height: 'fit-content'
    }, "-=0.2");
  }

  switchHover.to(switcher.line, 0.2, {
    height: '100%'
  }, "-=0.3");

  switchHover.to(switcher.hoveredText, 0.1, {
    top: "0px",
    opacity: "1",
    delay: '0.1'
  }, "-=0.1");
  console.log('finish show')
}

function hideBgSwitchAnimation(switcher){
  var switchHideHover = new TimelineMax();
  console.log('start hide')
  switchHideHover.to(switcher.hoveredText, 0.1, {
    top: "60px",
    opacity: "0",
  });

  switchHideHover.to(switcher.line, 0.2, {
    height: "0"
  }, "-=0.2");

  switchHideHover.to(switcher.smallText, 0.1, {
    display: "flex",
    opacity: "0.5",
    y: "0"
  }, "-=0.2");

  if (width>680){
    switchHideHover.to(switcher.parent, 0.1, {
      height: '20',
      delay: '0.1'
    }, "-=0.2");
  } else {
    switchHideHover.to(switcher.parent, 0.1, {
      height: 'fit-content',
      delay: '0.1'
    }, "-=0.2");
  }
  console.log('finish hide')
}

function overBgSwitchAnimation(switcher){
  // switchHover.to(switcher.smallText, 0.2, {
  //   opacity: "0",
  //   y: "-50",
  // });
  //
  // switchHover.to(switcher.parent, 0.2, {
  //   height: '80px'
  // }, "-=0.2");
  //
  // switchHover.to(switcher.line, 0.3, {
  //   height: '100%'
  // }, "-=0.3");
  //
  // switchHover.to(switcher.hoveredText, 0.2, {
  //   top: "0px",
  //   opacity: "1",
  //   delay: '0.1'
  // }, "-=0.1");
}

function outBgSwitchAnimation(switcher){
  // switchHideHover.to(switcher.hoveredText, 0.2, {
  //   top: "60px",
  //   opacity: "0",
  // });
  //
  // switchHideHover.to(switcher.line, 0.3, {
  //   height: "0"
  // }, "-=0.2");
  //
  // switchHideHover.to(switcher.smallText, 0.2, {
  //   opacity: "0.5",
  //   y: "0"
  // }, "-=0.2");
  //
  // switchHideHover.to(switcher.parent, 0.2, {
  //   height: '20px',
  //   delay: '0.1'
  // }, "-=0.2");
}
