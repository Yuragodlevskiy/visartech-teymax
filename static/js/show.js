var shownElements = document.querySelectorAll('[shown]')

shownElements.forEach((item, i) => {
  item.style.opacity = '0';
});

window.onload = showElements;
function showElements(){
  shownElements.forEach((item, i) => {
    shown = item.getAttribute("shown")
    if(Visible(item) && shown !== "true"){
      switch(item.getAttribute("shown_type")){
        case "fromTop":
          item.getAttribute("shown_delay")
            ? showFromTop(item, parseInt(shown), parseInt(item.getAttribute("shown_delay")))
            : showFromTop(item, parseInt(shown));
          break;
        case "fromBottom":
          item.getAttribute("shown_delay")
            ? showFromBottom(item, parseInt(shown), parseInt(item.getAttribute("shown_delay")))
            : showFromBottom(item, parseInt(shown));
          break;
        default:
          item.getAttribute("shown_delay")
            ? showFromBottom(item, parseInt(shown), parseInt(item.getAttribute("shown_delay")))
            : showFromBottom(item, parseInt(shown));
      }

      item.setAttribute("shown", true);
    }
  });
}

const Visible = function (target) {
  let defaultOffsetTop = 150;
  let tabletWidth = window.outerWidth < 768;
  (target.classList.contains('about__title') && tabletWidth) ? (defaultOffsetTop = 50) : 150;
  (target.classList.contains('about__desc') && tabletWidth) ? (defaultOffsetTop = 50) : 150;
  // Все позиции элемента
  const targetPosition = {
      top: window.pageYOffset + target.getBoundingClientRect().top,
      left: window.pageXOffset + target.getBoundingClientRect().left,
      right: window.pageXOffset + target.getBoundingClientRect().right,
      bottom: window.pageYOffset + target.getBoundingClientRect().bottom
    },
    // Получаем позиции окна
    windowPosition = {
      top: window.pageYOffset,
      left: window.pageXOffset,
      right: window.pageXOffset + document.documentElement.clientWidth,
      bottom: window.pageYOffset + document.documentElement.clientHeight
    };

  if ((targetPosition.top + defaultOffsetTop) < (windowPosition.top + window.innerHeight)) {
      return true;
  } else {
    console.clear()
    return false;
  }
};
