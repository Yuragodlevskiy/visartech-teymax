<?php

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyAkeI2SUhaEZXkA3ZLeFhcp40biynVYxgY');
}

add_action('acf/init', 'my_acf_init');

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title'    => __('Options'),
		'menu_slug' => 'options',
		'position'    => 50
	));

	acf_add_options_sub_page(array(
		'page_title'	=> __('Contacts'),
		'post_id'		=> 'contacts',
		'parent_slug' 	=> 'options'
	));

	acf_add_options_sub_page(array(
		'page_title'	=> __('Blog Subscribe Form'),
		'post_id'		=> 'blog_subscribe_form',
		'parent_slug' 	=> 'options'
	));
}

wp_oembed_add_provider( '#https?://sketchfab\.com/.*#i', 'https://www.sketchfab.com/oembed', true );

/* Do something with the data entered */
add_action( 'save_post', 'update_minutes_to_read' );

/* When the post is saved, saves our custom data */
function update_minutes_to_read( $post_id ) {
  // First we need to check if the current user is authorised to do this action. 
  if ( 'page' == $_POST['post_type'] ) {
    if ( ! current_user_can( 'edit_page', $post_id ) )
      return;
  } else {
    if ( ! current_user_can( 'edit_post', $post_id ) )
      return;
  }
  
  $body = get_post_meta( $post_id, 'body', true );
  $dom = new DOMDocument();
  $dom->loadHTML( $body );
  $text = $dom->textContent;
  $words_count = str_word_count( $text );
  $word_minutes = $words_count / 275;
  
  $images = $dom->getElementsByTagName('img');
  $images_count = count($images);
  $image_minutes = calculate_images_minutes( $images_count );
  
  $total_minutes = ceil( $word_minutes + $image_minutes );
	update_post_meta( $post_id, 'minutes_to_read', $total_minutes );
}

function calculate_images_minutes( $imagesCount ) {
  $totalSeconds = 0;
  
  for ($i = 0; $i < $imagesCount; $i++) {
    if ($i < 10) {
      $totalSeconds += 12 - $i;
    } else {
      $totalSeconds += 3;
    }
  }
  
  return $totalSeconds / 60;
}