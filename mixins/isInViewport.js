export default {
  data() {
    return {
      elementIsInViewport: false,
      elementViewportOffset: -400
    }
  },
  methods: {
    handleScroll() {
      const rect = this.$el.getBoundingClientRect()
      const scrollTop = window.pageYOffset || document.documentElement.scrollTop
      const yPos = parseInt(rect.top + scrollTop)
      const isInViewport =
        yPos <
        parseInt(scrollTop + (window.innerHeight - this.elementViewportOffset))
      if (isInViewport) {
        window.removeEventListener('scroll', this.handleScroll, this)
        this.elementIsInViewport = isInViewport
      }
    }
  },
  computed: {
    isInViewport() {
      return this.elementIsInViewport
    }
  },
  mounted() {
    this.handleScroll()
    window.addEventListener('scroll', this.handleScroll)
  },
  beforeDestroy() {
    window.removeEventListener('scroll', this.handleScroll, this)
  }
}
