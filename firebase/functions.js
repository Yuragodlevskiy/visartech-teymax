const functions = require('firebase-functions')
const admin = require('firebase-admin')

const serviceAccount = require('./visartech-website-firebase-adminsdk-jyyeu-79e8b2fec0.json')
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://visartech-website.firebaseio.com'
})

const db = admin.database()
exports.clap = functions.https.onCall((data, context) => {
  const postSlug = data.postSlug
  const clapsRef = db.ref('blog/claps').child(postSlug)
  clapsRef.transaction((currentClaps) => {
    if (!currentClaps) currentClaps = 0

    return currentClaps + 1
  })
})
