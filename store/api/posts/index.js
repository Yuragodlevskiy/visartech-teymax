export const state = () => ({})

export const mutations = {}

export const actions = {
  async fetch({ dispatch }) {
    await Promise.all([
      dispatch('posts/fetch'),
      dispatch('portfolio/fetch'),
      dispatch('service-directions/fetch'),
      dispatch('businesses/fetch'),
      // dispatch('industries/fetch'),
      // dispatch('technology-groups/fetch'),
      // dispatch('history-years/fetch'),
      dispatch('clients/fetch'),
      dispatch('hot-links/fetch')
    ])
  }
}
