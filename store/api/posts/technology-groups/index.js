const initialState = {
  technologyGroups: []
}

export const state = () => ({
  ...initialState
})

export const getters = {
  technologyGroups(state) {
    return state.technologyGroups
  }
}

const MUTATION_SET_TECHNOLOGY_GROUPS = 'setTechnologyGroups'

export const actions = {
  async fetch({ commit }) {
    const technologyGroups = await this.$axios.$get(
      '/acf/v3/technology_group?per_page=100',
      { useCache: process.env.cacheAPIRequests }
    )
    commit(MUTATION_SET_TECHNOLOGY_GROUPS, technologyGroups)
  }
}

export const mutations = {
  [MUTATION_SET_TECHNOLOGY_GROUPS](state, technologyGroups) {
    state.technologyGroups = technologyGroups
  }
}
