const initialState = {
  portfolioItems: [],
  portfolioServices: [],
  portfolioIndustries: [],
  portfolioTechnologies: []
}

export const state = () => ({
  ...initialState
})

export const getters = {
  portfolioItems(state) {
    return state.portfolioItems
  },
  portfolioServices(state) {
    return state.portfolioServices
  },
  portfolioIndustries(state) {
    return state.portfolioIndustries
  },
  portfolioTechnologies(state) {
    return state.portfolioTechnologies
  }
}

const MUTATION_SET_PORTFOLIO_ITEMS = 'setPortfolioItems'
const MUTATION_SET_PORTFOLIO_SERVICES = 'setPortfolioServices'
const MUTATION_SET_PORTFOLIO_INDUSTRIES = 'setPortfolioIndustries'
const MUTATION_SET_PORTFOLIO_TECHNOLOGIES = 'setPortfolioTechnologies'

const ACTION_FETCH_PORTFOLIO_ITEMS = 'fetchPortfolioItems'
const ACTION_FETCH_PORTFOLIO_SERVICES = 'fetchPortfolioServices'
const ACTION_FETCH_PORTFOLIO_INDUSTRIES = 'fetchPortfolioIndustries'
const ACTION_FETCH_PORTFOLIO_TECHNOLOGIES = 'fetchPortfolioTechnologies'

export const actions = {
  async fetch({ dispatch }) {
    await Promise.all([
      dispatch(ACTION_FETCH_PORTFOLIO_ITEMS),
      dispatch(ACTION_FETCH_PORTFOLIO_SERVICES),
      dispatch(ACTION_FETCH_PORTFOLIO_INDUSTRIES),
      dispatch(ACTION_FETCH_PORTFOLIO_TECHNOLOGIES)
    ])
  },

  async [ACTION_FETCH_PORTFOLIO_ITEMS]({ commit }) {
    const portfolioItems = await this.$axios.$get(
      '/acf/v3/portfolio_item?per_page=100',
      {
        useCache: process.env.cacheAPIRequests
      }
    )
    commit(MUTATION_SET_PORTFOLIO_ITEMS, portfolioItems)
  },
  async [ACTION_FETCH_PORTFOLIO_SERVICES]({ commit }) {
    const portfolioServices = await this.$axios.$get(
      '/wp/v2/portfolio_service?per_page=100',
      { useCache: process.env.cacheAPIRequests }
    )
    commit(MUTATION_SET_PORTFOLIO_SERVICES, portfolioServices)
  },
  async [ACTION_FETCH_PORTFOLIO_INDUSTRIES]({ commit }) {
    const portfolioIndustries = await this.$axios.$get(
      '/wp/v2/portfolio_industry?per_page=100',
      { useCache: process.env.cacheAPIRequests }
    )
    commit(MUTATION_SET_PORTFOLIO_INDUSTRIES, portfolioIndustries)
  },
  async [ACTION_FETCH_PORTFOLIO_TECHNOLOGIES]({ commit }) {
    const portfolioTechnologies = await this.$axios.$get(
      '/wp/v2/portfolio_technology?per_page=100',
      { useCache: process.env.cacheAPIRequests }
    )
    commit(MUTATION_SET_PORTFOLIO_TECHNOLOGIES, portfolioTechnologies)
  }
}

export const mutations = {
  [MUTATION_SET_PORTFOLIO_ITEMS](state, portfolioItems) {
    state.portfolioItems = portfolioItems
  },
  [MUTATION_SET_PORTFOLIO_SERVICES](state, portfolioServices) {
    state.portfolioServices = portfolioServices
  },
  [MUTATION_SET_PORTFOLIO_INDUSTRIES](state, portfolioIndustries) {
    state.portfolioIndustries = portfolioIndustries
  },
  [MUTATION_SET_PORTFOLIO_TECHNOLOGIES](state, portfolioTechnologies) {
    state.portfolioTechnologies = portfolioTechnologies
  }
}
