const initialState = {
  clients: []
}

export const state = () => ({
  ...initialState
})

export const getters = {
  clients(state) {
    return state.clients
  }
}

const MUTATION_SET_CLIENTS = 'setClients'

export const actions = {
  async fetch({ commit }) {
    const clients = await this.$axios.$get('/acf/v3/client?per_page=100', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_CLIENTS, clients)
  }
}

export const mutations = {
  [MUTATION_SET_CLIENTS](state, clients) {
    state.clients = clients
  }
}
