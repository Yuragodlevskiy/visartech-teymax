const initialState = {
  businesses: []
}

export const state = () => ({
  ...initialState
})

export const getters = {
  businesses(state) {
    return state.businesses
  }
}

const MUTATION_SET_BUSINESSES = 'setBusinesses'

export const actions = {
  async fetch({ commit }) {
    const businesses = await this.$axios.$get('/acf/v3/business?per_page=100', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_BUSINESSES, businesses)
  }
}

export const mutations = {
  [MUTATION_SET_BUSINESSES](state, businesses) {
    state.businesses = businesses
  }
}
