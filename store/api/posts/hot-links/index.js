const initialState = {
  hotLinks: []
}

export const state = () => ({
  ...initialState
})

export const getters = {
  hotLinks(state) {
    return state.hotLinks
  }
}

const MUTATION_SET_HOT_LINKS = 'setHotLinks'

export const actions = {
  async fetch({ commit }) {
    const hotLinks = await this.$axios.$get('/acf/v3/hot_link?per_page=100', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_HOT_LINKS, hotLinks)
  }
}

export const mutations = {
  [MUTATION_SET_HOT_LINKS](state, hotLinks) {
    state.hotLinks = hotLinks
  }
}
