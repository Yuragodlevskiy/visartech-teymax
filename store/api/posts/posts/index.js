const initialState = {
  posts: [],
  categories: [],
  tags: [],
  authors: []
}

export const state = () => ({
  ...initialState
})

export const getters = {
  posts(state) {
    return state.posts
  },
  categories(state) {
    return state.categories
  },
  tags(state) {
    return state.tags
  },
  authors(state) {
    return state.authors
  }
}

const MUTATION_SET_POSTS = 'setPosts'
const MUTATION_SET_CATEGORIES = 'setCategories'
const MUTATION_SET_TAGS = 'setTags'
const MUTATION_SET_AUTHORS = 'setAuthors'

const ACTION_FETCH_POSTS = 'fetchPosts'
const ACTION_FETCH_CATEGORIES = 'fetchCategories'
const ACTION_FETCH_TAGS = 'fetchTags'
const ACTION_FETCH_AUTHORS = 'fetchAuthors'

export const actions = {
  async fetch({ dispatch }) {
    await Promise.all([
      dispatch(ACTION_FETCH_POSTS),
      dispatch(ACTION_FETCH_CATEGORIES),
      dispatch(ACTION_FETCH_TAGS)
      // dispatch(ACTION_FETCH_AUTHORS)
    ])
  },

  async [ACTION_FETCH_POSTS]({ commit }) {
    const posts = await this.$axios.$get('/wp/v2/posts?per_page=100', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_POSTS, posts)
  },
  async [ACTION_FETCH_CATEGORIES]({ commit }) {
    const categories = await this.$axios.$get(
      '/wp/v2/categories?per_page=100',
      {
        useCache: process.env.cacheAPIRequests
      }
    )
    commit(MUTATION_SET_CATEGORIES, categories)
  },
  async [ACTION_FETCH_TAGS]({ commit }) {
    const tags = await this.$axios.$get('/wp/v2/tags?per_page=100', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_TAGS, tags)
  },
  async [ACTION_FETCH_AUTHORS]({ commit }) {
    const authors = await this.$axios.$get('/wp/v2/blog_author?per_page=100', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_AUTHORS, authors)
  }
}

export const mutations = {
  [MUTATION_SET_POSTS](state, posts) {
    state.posts = posts
  },
  [MUTATION_SET_CATEGORIES](state, categories) {
    state.categories = categories
  },
  [MUTATION_SET_TAGS](state, tags) {
    state.tags = tags
  },
  [MUTATION_SET_AUTHORS](state, authors) {
    state.authors = authors
  }
}
