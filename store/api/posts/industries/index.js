const initialState = {
  industries: []
}

export const state = () => ({
  ...initialState
})

export const getters = {
  industries(state) {
    return state.industries
  }
}

const MUTATION_SET_INDUSTRIES = 'setIndustries'

export const actions = {
  async fetch({ commit }) {
    const industries = await this.$axios.$get('/acf/v3/industry?per_page=100', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_INDUSTRIES, industries)
  }
}

export const mutations = {
  [MUTATION_SET_INDUSTRIES](state, industries) {
    state.industries = industries
  }
}
