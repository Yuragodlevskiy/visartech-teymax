const initialState = {
  serviceDirections: []
}

export const state = () => ({
  ...initialState
})

export const getters = {
  serviceDirections(state) {
    return state.serviceDirections
  }
}

const MUTATION_SET_SERVICE_DIRECTIONS = 'setServiceDirections'

export const actions = {
  async fetch({ commit }) {
    const serviceDirections = await this.$axios.$get(
      '/acf/v3/service_direction?per_page=100',
      { useCache: process.env.cacheAPIRequests }
    )
    commit(MUTATION_SET_SERVICE_DIRECTIONS, serviceDirections)
  }
}

export const mutations = {
  [MUTATION_SET_SERVICE_DIRECTIONS](state, serviceDirections) {
    state.serviceDirections = serviceDirections
  }
}
