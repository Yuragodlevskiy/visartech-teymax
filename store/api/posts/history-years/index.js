const initialState = {
  historyYears: []
}

export const state = () => ({
  ...initialState
})

export const getters = {
  historyYears(state) {
    return state.historyYears
  }
}

const MUTATION_SET_HISTORY_YEARS = 'setHistoryYears'

export const actions = {
  async fetch({ commit }) {
    const historyYears = await this.$axios.$get(
      '/acf/v3/history_year?per_page=100',
      {
        useCache: process.env.cacheAPIRequests
      }
    )
    commit(MUTATION_SET_HISTORY_YEARS, historyYears)
  }
}

export const mutations = {
  [MUTATION_SET_HISTORY_YEARS](state, historyYears) {
    state.historyYears = historyYears
  }
}
