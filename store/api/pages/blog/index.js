const initialState = {
  blogPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  blogPage(state) {
    return state.blogPage
  }
}

const MUTATION_SET_BLOG_PAGE = 'setBlogPage'

export const actions = {
  async fetch({ commit }) {
    const blogPage = await this.$axios.$get('/acf/v3/pages/46', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_BLOG_PAGE, blogPage)
  }
}

export const mutations = {
  [MUTATION_SET_BLOG_PAGE](state, blogPage) {
    state.blogPage = blogPage
  }
}
