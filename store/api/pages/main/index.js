const initialState = {
  mainPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  mainPage(state) {
    return state.mainPage
  }
}

const MUTATION_SET_MAIN_PAGE = 'setMainPage'

export const actions = {
  async fetch({ commit }) {
    const mainPage = await this.$axios.$get('/acf/v3/pages/20', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_MAIN_PAGE, mainPage)
  }
}

export const mutations = {
  [MUTATION_SET_MAIN_PAGE](state, mainPage) {
    state.mainPage = mainPage
  }
}
