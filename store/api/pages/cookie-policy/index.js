const initialState = {
  cookiePolicyPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  cookiePolicyPage(state) {
    return state.cookiePolicyPage
  }
}

const MUTATION_SET_DIGITAL_ART_PAGE = 'setCookiePolicyPage'

export const actions = {
  async fetch({ commit }) {
    const cookiePolicyPage = await this.$axios.$get('/acf/v3/pages/69', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_DIGITAL_ART_PAGE, cookiePolicyPage)
  }
}

export const mutations = {
  [MUTATION_SET_DIGITAL_ART_PAGE](state, cookiePolicyPage) {
    state.cookiePolicyPage = cookiePolicyPage
  }
}
