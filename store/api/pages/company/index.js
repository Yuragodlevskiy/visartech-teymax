const initialState = {
  companyPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  companyPage(state) {
    return state.companyPage
  }
}

const MUTATION_SET_COMPANY_PAGE = 'setCompanyPage'

export const actions = {
  async fetch({ commit }) {
    const companyPage = await this.$axios.$get('/acf/v3/pages/48', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_COMPANY_PAGE, companyPage)
  }
}

export const mutations = {
  [MUTATION_SET_COMPANY_PAGE](state, companyPage) {
    state.companyPage = companyPage
  }
}
