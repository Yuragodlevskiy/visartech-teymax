const initialState = {
  startupsPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  startupsPage(state) {
    return state.startupsPage
  }
}

const MUTATION_SET_STARTUPS_PAGE = 'setStartupsPage'

export const actions = {
  async fetch({ commit }) {
    const startupsPage = await this.$axios.$get('/acf/v3/pages/62', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_STARTUPS_PAGE, startupsPage)
  }
}

export const mutations = {
  [MUTATION_SET_STARTUPS_PAGE](state, startupsPage) {
    state.startupsPage = startupsPage
  }
}
