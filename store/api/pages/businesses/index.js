const initialState = {
  businessesPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  businessesPage(state) {
    return state.businessesPage
  }
}

const MUTATION_SET_BUSINESSES_PAGE = 'setBusinessesPage'
const ACTION_FETCH_BUSINESSES_PAGE = 'fetchBusinessesPage'

export const actions = {
  async fetch({ dispatch }) {
    await Promise.all([
      dispatch(ACTION_FETCH_BUSINESSES_PAGE),

      dispatch('startups/fetch'),
      dispatch('enterprises/fetch'),
      dispatch('gaming/fetch')
    ])
  },

  async [ACTION_FETCH_BUSINESSES_PAGE]({ commit }) {
    const businessesPage = await this.$axios.$get('/acf/v3/pages/42', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_BUSINESSES_PAGE, businessesPage)
  }
}

export const mutations = {
  [MUTATION_SET_BUSINESSES_PAGE](state, businessesPage) {
    state.businessesPage = businessesPage
  }
}
