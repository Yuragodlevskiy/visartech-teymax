const initialState = {
  gamingPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  gamingPage(state) {
    return state.gamingPage
  }
}

const MUTATION_SET_GAMING_PAGE = 'setGamingPage'

export const actions = {
  async fetch({ commit }) {
    const gamingPage = await this.$axios.$get('/acf/v3/pages/66', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_GAMING_PAGE, gamingPage)
  }
}

export const mutations = {
  [MUTATION_SET_GAMING_PAGE](state, gamingPage) {
    state.gamingPage = gamingPage
  }
}
