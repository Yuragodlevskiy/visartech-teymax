const initialState = {
  portfolioPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  portfolioPage(state) {
    return state.portfolioPage
  }
}

const MUTATION_SET_PORTFOLIO_PAGE = 'setPortfolioPage'

export const actions = {
  async fetch({ commit }) {
    const portfolioPage = await this.$axios.$get('/acf/v3/pages/22', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_PORTFOLIO_PAGE, portfolioPage)
  }
}

export const mutations = {
  [MUTATION_SET_PORTFOLIO_PAGE](state, portfolioPage) {
    state.portfolioPage = portfolioPage
  }
}
