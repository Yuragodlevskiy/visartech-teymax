const initialState = {
  privacyPolicyPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  privacyPolicyPage(state) {
    return state.privacyPolicyPage
  }
}

const MUTATION_SET_PRIVACY_POLICY_PAGE = 'setPrivacyPolicyPage'

export const actions = {
  async fetch({ commit }) {
    const privacyPolicyPage = await this.$axios.$get('/acf/v3/pages/626', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_PRIVACY_POLICY_PAGE, privacyPolicyPage)
  }
}

export const mutations = {
  [MUTATION_SET_PRIVACY_POLICY_PAGE](state, privacyPolicyPage) {
    state.privacyPolicyPage = privacyPolicyPage
  }
}
