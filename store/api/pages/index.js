export const state = () => ({})

export const mutations = {}

export const actions = {
  async fetch({ dispatch }) {
    await Promise.all([
      dispatch('main/fetch'),
      dispatch('portfolio/fetch'),
      dispatch('services/fetch'),
      dispatch('businesses/fetch'),
      // dispatch('industries/fetch'),
      dispatch('blog/fetch'),
      // dispatch('company/fetch'),
      dispatch('contacts/fetch'),
      dispatch('cookie-policy/fetch'),
      dispatch('privacy-policy/fetch')
    ])
  }
}
