const initialState = {
  industriesPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  industriesPage(state) {
    return state.industriesPage
  }
}

const MUTATION_SET_INDUSTRIES_PAGE = 'setIndustriesPage'

export const actions = {
  async fetch({ commit }) {
    const industriesPage = await this.$axios.$get('/acf/v3/pages/44', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_INDUSTRIES_PAGE, industriesPage)
  }
}

export const mutations = {
  [MUTATION_SET_INDUSTRIES_PAGE](state, industriesPage) {
    state.industriesPage = industriesPage
  }
}
