const initialState = {
  videoGamesPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  videoGamesPage(state) {
    return state.videoGamesPage
  }
}

const MUTATION_SET_VIDEO_GAMES_PAGE = 'setVideoGamesPage'

export const actions = {
  async fetch({ commit }) {
    const videoGamesPage = await this.$axios.$get('/acf/v3/pages/58', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_VIDEO_GAMES_PAGE, videoGamesPage)
  }
}

export const mutations = {
  [MUTATION_SET_VIDEO_GAMES_PAGE](state, videoGamesPage) {
    state.videoGamesPage = videoGamesPage
  }
}
