const initialState = {
  interactiveAppPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  interactiveAppPage(state) {
    return state.interactiveAppPage
  }
}

const MUTATION_SET_INTERACTIVE_APP_PAGE = 'setInteractiveAppPage'

export const actions = {
  async fetch({ commit }) {
    const interactiveAppPage = await this.$axios.$get('/acf/v3/pages/52', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_INTERACTIVE_APP_PAGE, interactiveAppPage)
  }
}

export const mutations = {
  [MUTATION_SET_INTERACTIVE_APP_PAGE](state, interactiveAppPage) {
    state.interactiveAppPage = interactiveAppPage
  }
}
