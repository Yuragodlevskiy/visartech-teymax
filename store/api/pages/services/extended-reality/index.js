const initialState = {
  extendedRealityPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  extendedRealityPage(state) {
    return state.extendedRealityPage
  }
}

const MUTATION_SET_EXTENDED_REALITY_PAGE = 'setExtendedRealityPage'

export const actions = {
  async fetch({ commit }) {
    const extendedRealityPage = await this.$axios.$get('/acf/v3/pages/56', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_EXTENDED_REALITY_PAGE, extendedRealityPage)
  }
}

export const mutations = {
  [MUTATION_SET_EXTENDED_REALITY_PAGE](state, extendedRealityPage) {
    state.extendedRealityPage = extendedRealityPage
  }
}
