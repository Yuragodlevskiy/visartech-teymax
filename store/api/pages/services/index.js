const initialState = {
  servicesPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  servicesPage(state) {
    return state.servicesPage
  }
}

const MUTATION_SET_SERVICES_PAGE = 'setServicesPage'
const ACTION_FETCH_SERVICES_PAGE = 'fetchServicesPage'

export const actions = {
  async fetch({ dispatch }) {
    await Promise.all([
      dispatch(ACTION_FETCH_SERVICES_PAGE),

      dispatch('interactive-app/fetch'),
      dispatch('digital-art/fetch'),
      dispatch('extended-reality/fetch'),
      dispatch('video-games/fetch'),
      dispatch('software-product/fetch')
    ])
  },

  async [ACTION_FETCH_SERVICES_PAGE]({ commit }) {
    const servicesPage = await this.$axios.$get('/acf/v3/pages/40', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_SERVICES_PAGE, servicesPage)
  }
}

export const mutations = {
  [MUTATION_SET_SERVICES_PAGE](state, servicesPage) {
    state.servicesPage = servicesPage
  }
}
