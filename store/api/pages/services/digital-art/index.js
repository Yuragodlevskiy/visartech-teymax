const initialState = {
  digitalArtPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  digitalArtPage(state) {
    return state.digitalArtPage
  }
}

const MUTATION_SET_DIGITAL_ART_PAGE = 'setDigitalArtPage'

export const actions = {
  async fetch({ commit }) {
    const digitalArtPage = await this.$axios.$get('/acf/v3/pages/54', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_DIGITAL_ART_PAGE, digitalArtPage)
  }
}

export const mutations = {
  [MUTATION_SET_DIGITAL_ART_PAGE](state, digitalArtPage) {
    state.digitalArtPage = digitalArtPage
  }
}
