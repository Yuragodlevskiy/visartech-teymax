const initialState = {
  softwareProductPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  softwareProductPage(state) {
    return state.softwareProductPage
  }
}

const MUTATION_SET_SOFTWARE_PRODUCT_PAGE = 'setSoftwareProductPage'

export const actions = {
  async fetch({ commit }) {
    const softwareProductPage = await this.$axios.$get('/acf/v3/pages/60', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_SOFTWARE_PRODUCT_PAGE, softwareProductPage)
  }
}

export const mutations = {
  [MUTATION_SET_SOFTWARE_PRODUCT_PAGE](state, softwareProductPage) {
    state.softwareProductPage = softwareProductPage
  }
}
