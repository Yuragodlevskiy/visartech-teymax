const initialState = {
  contactsPage: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  contactsPage(state) {
    return state.contactsPage
  }
}

const MUTATION_SET_CONTACTS_PAGE = 'setContactsPage'

export const actions = {
  async fetch({ commit }) {
    const contactsPage = await this.$axios.$get('/acf/v3/pages/50', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_CONTACTS_PAGE, contactsPage)
  }
}

export const mutations = {
  [MUTATION_SET_CONTACTS_PAGE](state, contactsPage) {
    state.contactsPage = contactsPage
  }
}
