export const state = () => ({})

export const mutations = {}

export const actions = {
  async fetch({ dispatch }) {
    await Promise.all([
      dispatch('pages/fetch'),
      dispatch('posts/fetch'),
      dispatch('options/fetch')
    ])
  }
}
