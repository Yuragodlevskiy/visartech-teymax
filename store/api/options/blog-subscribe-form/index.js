const initialState = {
  blogSubscribeForm: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  blogSubscribeForm(state) {
    return state.blogSubscribeForm
  }
}

const MUTATION_SET_BLOG_SUBSCRIBE_FORM = 'setBlogSubscribeForm'

export const actions = {
  async fetch({ commit }) {
    const blogSubscribeForm = await this.$axios.$get(
      '/acf/v3/options/blog_subscribe_form',
      { useCache: process.env.cacheAPIRequests }
    )
    commit(MUTATION_SET_BLOG_SUBSCRIBE_FORM, blogSubscribeForm)
  }
}

export const mutations = {
  [MUTATION_SET_BLOG_SUBSCRIBE_FORM](state, blogSubscribeForm) {
    state.blogSubscribeForm = blogSubscribeForm
  }
}
