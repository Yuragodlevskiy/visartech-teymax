const initialState = {
  contacts: {}
}

export const state = () => ({
  ...initialState
})

export const getters = {
  contacts(state) {
    return state.contacts
  }
}

const MUTATION_SET_CONTACTS = 'setContacts'

export const actions = {
  async fetch({ commit }) {
    const contacts = await this.$axios.$get('/acf/v3/options/contacts', {
      useCache: process.env.cacheAPIRequests
    })
    commit(MUTATION_SET_CONTACTS, contacts)
  }
}

export const mutations = {
  [MUTATION_SET_CONTACTS](state, contacts) {
    state.contacts = contacts
  }
}
