export const state = () => ({})

export const mutations = {}

export const actions = {
  async fetch({ dispatch }) {
    await Promise.all([
      dispatch('contacts/fetch'),
      dispatch('blog-subscribe-form/fetch')
    ])
  }
}
