const initialState = {
  showThankYou: false
}

export const state = () => ({
  ...initialState
})

export const getters = {
  showThankYou(state) {
    return state.showThankYou
  }
}

export const mutations = {
  setShowThankYou(state, showThankYou) {
    state.showThankYou = showThankYou
  }
}

export const actions = {
  async nuxtServerInit({ dispatch }) {
    await dispatch('api/fetch')
  }
}
