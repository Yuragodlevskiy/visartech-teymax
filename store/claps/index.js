import { fireDb, fireFunc } from '@/plugins/firebase'

const initialState = {
  claps: {},
  localLikedPostSlugs: [],
  lastFetchTime: null
}

export const state = () => ({
  ...initialState
})

export const getters = {
  claps(state) {
    return state.claps
  },
  localLikedPostSlugs(state) {
    return state.localLikedPostSlugs
  }
}

export const mutations = {
  setClaps(state, claps) {
    state.claps = claps
  },
  incrementClaps(state, postSlug) {
    if (postSlug in state.claps) {
      state.claps[postSlug]++
    } else {
      state.claps = { ...state.claps, [postSlug]: 1 }
    }
  },
  setLocalLikedPostSlugs(state, slug) {
    state.localLikedPostSlugs.push(slug)
  },
  setLastFetchTime(state, lastFetchTime) {
    state.lastFetchTime = lastFetchTime
  }
}

export const actions = {
  async fetchClaps({ commit, state }) {
    // Update not more often than once per minute
    if (state.lastFetchTime && new Date() - state.lastFetchTime < 1000 * 60)
      return

    const snapshot = await fireDb.ref('blog/claps').once('value')
    const claps = snapshot.val() || {}
    commit('setClaps', claps)
    commit('setLastFetchTime', new Date())
  },
  async clap({ commit, state }, postSlug) {
    const likedPostSlugs = state.localLikedPostSlugs
    if (likedPostSlugs.includes(postSlug)) return

    commit('incrementClaps', postSlug)

    commit('setLocalLikedPostSlugs', postSlug)

    const clapFunc = fireFunc.httpsCallable('clap')
    await clapFunc({ postSlug })
  }
}
